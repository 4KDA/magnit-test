// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
    // meta
    app: {
        head: {
            title: "Поиск GIF",
            meta: [
                {name: "viewport", content: "width=device-width, initial-scale=1"},
                {
                    hid: "description",
                    name: "description",
                    content: "Поиск GIF по API GIPHY",
                },
            ],
            link: [
                {rel: "icon", type: "image/x-icon", href: "/favicon.ico"},
                {
                    rel: 'preconnect',
                    href: 'https://fonts.gstatic.com'
                },
                {
                    rel: 'stylesheet',
                    href: 'https://fonts.googleapis.com/css2?family=Rubik:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap'
                },
            ],
        },
    },
    // css
    css: [
        "~/app/index.scss",
    ],
    // modules
    modules: [
        '@nuxtjs/tailwindcss',
    ],
    tailwindcss: {
        cssPath: '~/app/styles/tailwind.scss',
        // configPath: 'tailwind.config',
    },
    runtimeConfig: {
        public: {
            giphyApiKey: process.env.NUXT_PUBLIC_GIPHY_KEY,
            giphyApiBase: process.env.NUXT_PUBLIC_GIPHY_API_BASE_URL || 'https://api.giphy.com/v1'
        }
    }
})
