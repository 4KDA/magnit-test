import type { Config } from 'tailwindcss'
export default <Partial<Config>>{
  content: [
    `./app/**/*.{vue,js,ts}`,
    `./layouts/**/*.{vue,js,ts}`,
    `./pages/**/*.{vue,js,ts}`,
    `./widgets/**/*.{vue,js,ts}`,
    `./features/**/*.{vue,js,ts}`,
    `./entities/**/*.{vue,js,ts}`,
    `./shared/**/*.{vue,js,ts}`,
  ],
  plugins: [require("@tailwindcss/forms")],
  theme: {
    extend: {
      fontFamily: {
        sans: ["Rubik", "sans-serif"],
      },
    }
  }
}
