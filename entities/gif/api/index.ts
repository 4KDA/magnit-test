import {GifsLang, GifsRating, IGif} from "~/entities/gif";
import {Ref} from "@vue/reactivity";

interface IResponseGifs {
    data: IGif[];
    pagination: IPagination;
    meta: IMeta;
}

interface IPagination {
    total_count: number;
    count: number;
    offset: number;
}

interface IMeta {
    status: number;
    msg: string;
    response_id: string;
}

export const fetchGifs = (params: {
    q: string | Ref<string>,
    limit?: number | Ref<number>,
    offset?: number | Ref<number>,
    rating?: GifsRating,
    lang?: GifsLang
}) => {
    const config = useRuntimeConfig()

    const paramsQuery = {
        limit: 25,
        offset: 0,
        rating: GifsRating.G,
        lang: GifsLang.RU,
        ...params,
    }

    return useLazyFetch<IResponseGifs>('/gifs/search', {
        baseURL: config.public.giphyApiBase,
        query: {
            api_key: config.public.giphyApiKey,
            ...paramsQuery,
        }
    })
}
